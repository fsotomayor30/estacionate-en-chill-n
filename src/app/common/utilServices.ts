import { Estacionamiento } from './../entities/estacionamiento.entity';
import { AppState } from './../app.state';
import { UsuarioLogin } from '../entities/usuarioLogin.entity';
import { Usuario } from './../entities/usuario.entity';
import { Administrador } from './../entities/administrador.entity';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Estacionado } from './../entities/estacionado.entity';
import { CuposEstacionamiento } from './../entities/cuposEstacionamiento.entity';

declare var $: any;

@Injectable({
  providedIn: 'root'
})

export class UtilsService {

  constructor(
    private store: Store<AppState>
  ){}

  buscarEstacionamientoPorId(idEstSimulacion: String, estacionamientos: Estacionamiento[]): Estacionamiento {
    let estacionamiento: Estacionamiento = estacionamientos.find((estacionamiento => estacionamiento.id == idEstSimulacion));
    return estacionamiento;
  }

  showNotificationExito(from, align, mensaje) {
    $.notify({
      icon: "pe-7s-like2",
      message: mensaje
    }, {
      type: 'success',
      timer: 1000,
      placement: {
        from: from,
        align: align
      }
    });
  }
  
  returnIdLogueado():string{
    let usuarioLogueado:UsuarioLogin;
    let usuarios: Usuario[]=[];
    let idUsuario:string;
    this.store.select('usuarioLogin').subscribe(
      (data:UsuarioLogin)=>{
        usuarioLogueado=data;
        this.store.select('usuarios').subscribe(
          (data:Usuario[]) =>{
            usuarios=data;
            usuarios.forEach(element => {
              if(element.email == usuarioLogueado.email){
                idUsuario = element.estacionamiento;
              }
            });
          }
        );
      });
      return idUsuario;
  }

  returnEstacionados(idLogueado:string): Estacionado[]{
    let estacionados:Estacionado[]=[];
    let estacionadosActivos:Estacionado[]=[]
    this.store.select('estacionados').subscribe(
      (data: Estacionado[]) => {
          estacionados = data;          
          let estacionadosTotal= estacionados.filter(autoEstacionado=>autoEstacionado.idEstacionamiento == idLogueado && autoEstacionado.estacionado);
          estacionadosActivos = estacionadosTotal;
      }
      
    );
    return estacionadosActivos;
  }

  returnEstacionamientoxID(IdEstacionamiento:string):Estacionamiento{
    let estacionamiento:Estacionamiento;
    this.store.select("estacionamientos").subscribe(
      (data:Estacionamiento[]) =>{
        let estacionadominetoFind:Estacionamiento = data.find(parkin=>parkin.id==IdEstacionamiento);
        estacionamiento = estacionadominetoFind;
      }
    );
    return estacionamiento;
  }
  
  returnCuposEstacionamientoxID(IdEstacionamiento:string):CuposEstacionamiento{
    let misCupos: CuposEstacionamiento;
    this.store.select("cuposEstacionamientos").subscribe(
      (data:CuposEstacionamiento[]) =>{
        misCupos = data.find(parkinCupos=> parkinCupos.idEstacionamiento==IdEstacionamiento);
        
      }
    );
    return misCupos;
  }


}
