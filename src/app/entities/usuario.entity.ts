export class Usuario {
    id?: string;
    email?: string;
    habilitado?: boolean;
    estacionamiento?: string;
}
