import { Estacionado } from "./estacionado.entity";

export class CuposEstacionamiento {
    idEstacionamiento?: string;
    cuposTotales:number;
    cuposDisponibles: number;
}
