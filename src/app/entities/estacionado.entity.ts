export class Estacionado {
    id: string="";
    patenteVehiculo:string="";
    fechaLlegada: string="";
    fechaRetirada: string="";
    estacionado: boolean=false;
    idEstacionamiento: string;
    pagoEstacionamineto?:number;
}
