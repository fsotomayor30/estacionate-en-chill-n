export class UsuarioLogin {
    displayName?:string;
    uid?: string;
    photoURL?: string;
    email?: string;
    rol?: string;
}
