export class Estacionamiento {
    direccion:string;
    esTechado: boolean;
    id?: string;
    latitud: number;
    longitud: number;
    nombre: string;
    precioPorMinuto:number;
    telefono: string;
    tieneCamaraSeguridad:boolean;
    habilitado:boolean;
}
