import { Injectable } from '@angular/core';
import { AppState } from './../../app.state';
import {Simulacion} from '../../entities/simulacion.entity'
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Store } from '@ngrx/store';
@Injectable({
  providedIn: 'root'
})
export class SimulacionService {
  private rutaColeccion = "Simulacion";

  public simulacionCollection: AngularFirestoreCollection<Simulacion>;
  public simulaciones: Observable<Simulacion[]>;
  public simulacionDoc: AngularFirestoreDocument<Simulacion>;

  constructor(public afs: AngularFirestore) { 

    this.simulacionCollection = this.afs.collection<Simulacion>(this.rutaColeccion);
    this.simulaciones = this.simulacionCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Simulacion;
        const id = a.payload.doc.id;
        return { id, ...data }
      })));    
  }

  obtenerSimulaciones(){
    return this.simulaciones;
  }

}
