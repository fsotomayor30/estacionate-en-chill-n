import { TestBed } from '@angular/core/testing';

import { EstacionadoService } from './estacionado.service';

describe('EstacionadoService', () => {
  let service: EstacionadoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EstacionadoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
