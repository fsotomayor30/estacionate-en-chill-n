import { AppState } from './../../app.state';
import { Observable } from 'rxjs';
import { Estacionado } from 'app/entities/estacionado.entity';
import { AngularFirestore,  AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EstacionadoService {

  private rutaColeccion = "VehiculosEstacionados";
  public estacionadoCollection: AngularFirestoreCollection<Estacionado>;
  public estacionados: Observable<Estacionado[]>;
  public estacionadoDoc: AngularFirestoreDocument<Estacionado>;


  constructor(
    public afs: AngularFirestore,
    private store: Store<AppState>,
  ) {
    this.estacionadoCollection = this.afs.collection<Estacionado>(this.rutaColeccion);
    this.estacionados = this.estacionadoCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Estacionado;
        const id = a.payload.doc.id;
        return { id, ...data }
      })));
  }

  obtenerEstacionados() {
    return this.estacionados;
  }

  editarEstacionado(estacionado: Estacionado) {
    this.estacionadoDoc = this.afs.doc(`${this.rutaColeccion}/${estacionado.id}`);
    this.estacionadoDoc.update(estacionado);
  }

   agregarEstacionado(estacionado: Estacionado) {
    var id = Math.random().toString(36).slice(-20)
    estacionado.id = id;
    return new Promise<any>((resolve, reject) => {
      this.estacionadoCollection.doc(id).set(estacionado)
        .then(res => { }, err => reject(err));
    });
  }
}
