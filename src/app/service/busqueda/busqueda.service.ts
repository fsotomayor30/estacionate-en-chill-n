import { Injectable } from '@angular/core';
import { AppState } from './../../app.state';
import {Busqueda} from '../../entities/busqueda.entity'
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Store } from '@ngrx/store';


@Injectable({
  providedIn: 'root'
})
export class BusquedaService {
  private rutaColeccion = "Busqueda";

  public busquedaCollection: AngularFirestoreCollection<Busqueda>;
  public busquedas: Observable<Busqueda[]>;
  public busquedaDoc: AngularFirestoreDocument<Busqueda>;

  constructor( public afs: AngularFirestore) { 
    this.busquedaCollection = this.afs.collection<Busqueda>(this.rutaColeccion);
    this.busquedas = this.busquedaCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Busqueda;
        const id = a.payload.doc.id;
        return { id, ...data }
      })));
  }

  obtenerBusquedas(){
    return this.busquedas;
  }
}
