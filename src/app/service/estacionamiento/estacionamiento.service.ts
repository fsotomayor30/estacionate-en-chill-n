import { UpdateEstacionamientoAction } from '../../components/estacionamientos/actions/estacionamiento.action';
import { AppState } from './../../app.state';
import { Estacionamiento } from './../../entities/estacionamiento.entity';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Store } from '@ngrx/store';

@Injectable({
  providedIn: 'root'
})
export class EstacionamientoService {

  private rutaColeccion = "Estacionamientos";
  public estacionamientoCollection: AngularFirestoreCollection<Estacionamiento>;
  public estacionamientos: Observable<Estacionamiento[]>;
  public estacionamientoDoc: AngularFirestoreDocument<Estacionamiento>;


  constructor(
    public afs: AngularFirestore,
    private store: Store<AppState>,
  ) {
    this.estacionamientoCollection = this.afs.collection<Estacionamiento>(this.rutaColeccion);
    this.estacionamientos = this.estacionamientoCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Estacionamiento;
        const id = a.payload.doc.id;
        return { id, ...data }
      })));
  }

  obtenerEstacionamientos() {
    return this.estacionamientos;
  }

  editarEstacionamiento(estacionamiento: Estacionamiento) {
    this.estacionamientoDoc = this.afs.doc(`${this.rutaColeccion}/${estacionamiento.id}`);
    this.estacionamientoDoc.update(estacionamiento);
  }

  agregarEstacionamiento(estacionamiento: Estacionamiento) {
    var id = Math.random().toString(36).slice(-20)
    estacionamiento.id = id;
    return new Promise<any>((resolve, reject) => {
      this.estacionamientoCollection.doc(id).set(estacionamiento)
        .then(res => { }, err => reject(err));
    });
  }
}
