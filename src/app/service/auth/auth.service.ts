import { Estacionado } from 'app/entities/estacionado.entity';
import { EstacionadoService } from './../estacionado/estacionado.service';
import { UpdateCupoEstacionamientoAction } from './../../components/cupos-estacionamiento/actions/cuposEstacionamiento.action';
import { UpdateEstacionadoAction } from './../../components/cupos-estacionamiento/actions/estacionado.action';
import { CuposEstacionamientoService } from './../cuposEstacionamiento/cupos-estacionamiento.service';
import { UsuarioLogin } from './../../entities/usuarioLogin.entity';
import { Usuario } from './../../entities/usuario.entity';
import { UsuarioService } from './../usuario/usuario.service';
import { UpdateBusquedaAction } from '../../components/busquedas/actions/busqueda.action';
import { UpdateEstacionamientoAction } from '../../components/estacionamientos/actions/estacionamiento.action';
import { Busqueda } from './../../entities/busqueda.entity';
import { BusquedaService } from './../busqueda/busqueda.service';
import { Administrador } from './../../entities/administrador.entity';
import { AdministradorService } from './../administrador/administrador.service';
import { Estacionamiento } from './../../entities/estacionamiento.entity';
import { EstacionamientoService } from './../estacionamiento/estacionamiento.service';
import { Router } from '@angular/router';
import { UpdateUserAction } from '../../components/login/actions/user.action';
import { AppState } from '../../app.state';
import { Injectable } from '@angular/core';
import { auth } from 'firebase/app';
import { AngularFireAuth } from "@angular/fire/auth";
import { Store } from '@ngrx/store';
import { UpdateAdministradorAction } from 'app/components/administradores/actions/administrador.action';
import { SimulacionService } from './../simulacion/simulacion.service';
import { Simulacion } from './../../entities/simulacion.entity';
import { UpdateSimulacionAction } from 'app/components/simulaciones/actions/simulacion.action';
import { UpdateUsuarioAction } from 'app/components/usuarios/actions/usuario.action';
import { CuposEstacionamiento } from 'app/entities/cuposEstacionamiento.entity';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    public afAuth: AngularFireAuth,
    private store: Store<AppState>,
    private router: Router,
    private estacionamientoService: EstacionamientoService,
    private administradorService: AdministradorService,
    private busquedaService: BusquedaService,
    private simulacionService:SimulacionService ,
    private usuarioService: UsuarioService,
    private cuposEstacionamiento: CuposEstacionamientoService,
    private estacionadoService: EstacionadoService
  ) { }

  GoogleAuth() {
    return this.AuthLogin(new auth.GoogleAuthProvider());
  }

  GoogleLogOut(){
    return this.afAuth.auth.signOut().then((result) => {
      console.log(result);
    })  
  }


  
  AuthLogin(provider) {
    return this.afAuth.auth.signInWithPopup(provider)
      .then((result) => {
        this.administradorService.obtenerAdministradores().subscribe(
          (data:Administrador[])=> {
            let administrador: Administrador = data.find(administrador => administrador.correo == result.user.email && administrador.habilitado==true);
            if(administrador!=null){
              var UsuarioLogin = {
                displayName: result.user.displayName,
                uid: result.user.uid,
                photoURL: result.user.photoURL,
                email: result.user.email,
                rol: "ADMINISTRADOR"
              }
              this.store.dispatch(
                new UpdateUserAction(UsuarioLogin)
              );
              
              this.administradorService.obtenerAdministradores().subscribe(
                (data: Administrador[]) => {
                  this.store.dispatch(
                    new UpdateAdministradorAction(data)
                  )
                }
              );

              this.estacionamientoService.obtenerEstacionamientos().subscribe(
                (data: Estacionamiento[]) => {
                  this.store.dispatch(
                    new UpdateEstacionamientoAction(data)
                  )
                }
              );

              this.busquedaService.obtenerBusquedas().subscribe(
                (data : Busqueda[]) => {
                  this.store.dispatch(
                    new UpdateBusquedaAction(data)
                  )
                }
              );

              this.simulacionService.obtenerSimulaciones().subscribe(
                (data : Simulacion[]) => {
                  this.store.dispatch(
                    new UpdateSimulacionAction(data)
                  )
                }
              );

              this.usuarioService.obtenerUsuarios().subscribe(
                (data: Usuario[]) => {
                  this.store.dispatch(
                    new UpdateUsuarioAction(data)
                  )
                }
              );
              
              this.cuposEstacionamiento.obtenerCuposEstacionamiento().subscribe(
                (data: CuposEstacionamiento[]) => {
                  this.store.dispatch(
                    new UpdateCupoEstacionamientoAction(data)
                  )
                }
              );

              this.estacionadoService.obtenerEstacionados().subscribe(
                (data: Estacionado[]) => {
                  this.store.dispatch(
                    new UpdateEstacionadoAction(data)
                  )
                }
              );
            }
          }
        );
        this.usuarioService.obtenerUsuarios().subscribe(
          (data:Usuario[])=> {
            let usuario: Usuario = data.find(usuario => usuario.email == result.user.email && usuario.habilitado==true);
            if(usuario!=null){
              var UsuarioLogin = {
                displayName: result.user.displayName,
                uid: result.user.uid,
                photoURL: result.user.photoURL,
                email: result.user.email,
                rol: "ESTACIONAMIENTO"
              }
              this.store.dispatch(
                new UpdateUserAction(UsuarioLogin)
              );
              this.administradorService.obtenerAdministradores().subscribe(
                (data: Administrador[]) => {
                  this.store.dispatch(
                    new UpdateAdministradorAction(data)
                  )
                }
              );

              this.estacionamientoService.obtenerEstacionamientos().subscribe(
                (data: Estacionamiento[]) => {
                  this.store.dispatch(
                    new UpdateEstacionamientoAction(data)
                  )
                }
              );

              this.busquedaService.obtenerBusquedas().subscribe(
                (data : Busqueda[]) => {
                  this.store.dispatch(
                    new UpdateBusquedaAction(data)
                  )
                }
              );

              this.simulacionService.obtenerSimulaciones().subscribe(
                (data : Simulacion[]) => {
                  this.store.dispatch(
                    new UpdateSimulacionAction(data)
                  )
                }
              );

              this.usuarioService.obtenerUsuarios().subscribe(
                (data: Usuario[]) => {
                  this.store.dispatch(
                    new UpdateUsuarioAction(data)
                  )
                }
              )

              this.cuposEstacionamiento.obtenerCuposEstacionamiento().subscribe(
                (data: CuposEstacionamiento[]) => {
                  this.store.dispatch(
                    new UpdateCupoEstacionamientoAction(data)
                  )
                }
              );

              this.estacionadoService.obtenerEstacionados().subscribe(
                (data: Estacionado[]) => {
                  this.store.dispatch(
                    new UpdateEstacionadoAction(data)
                  )
                }
              );
            }
          })

        this.store.select('usuarioLogin').subscribe(
          (data:UsuarioLogin)=>{
            if(data!=null){
              this.router.navigate(['/dashboard']);

            }else{
              this.router.navigate(['/login']);
            }
          }
        )

      }).catch((error) => {
        console.log(error)
      })
  }
}
