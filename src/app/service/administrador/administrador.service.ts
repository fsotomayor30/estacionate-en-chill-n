import { Observable } from 'rxjs';
import { Administrador } from './../../entities/administrador.entity';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdministradorService {

  private rutaColeccion = "Administradores";
  public administradorCollection: AngularFirestoreCollection<Administrador>;
  public administradores: Observable<Administrador[]>;
  public administradorDoc: AngularFirestoreDocument<Administrador>;

  constructor(
    public afs: AngularFirestore,
  ) {
    this.administradorCollection = this.afs.collection<Administrador>(this.rutaColeccion);
    this.administradores = this.administradorCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Administrador;
        const id = a.payload.doc.id;
        return { id, ...data }
      })));
  }

  obtenerAdministradores() {
    return this.administradores;
  }

  editarAdministrador(administrador: Administrador) {
    this.administradorDoc = this.afs.doc(`${this.rutaColeccion}/${administrador.id}`);
    this.administradorDoc.update(administrador);
  }

  agregarAdminitrador(administrador: Administrador) {
    var id = Math.random().toString(36).slice(-20)
    administrador.id = id;
    return new Promise<any>((resolve, reject) => {
      this.administradorCollection.doc(id).set(administrador)
        .then(res => { }, err => reject(err));
    });
  }
}
