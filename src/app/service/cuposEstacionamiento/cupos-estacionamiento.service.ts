import { AppState } from './../../app.state';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { CuposEstacionamiento } from './../../entities/cuposEstacionamiento.entity';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import { Estacionado } from 'app/entities/estacionado.entity';

@Injectable({
  providedIn: 'root'
})
export class CuposEstacionamientoService {

  private rutaColeccion = "CuposEstacionamiento";
  public cupoEstacionamientoCollection: AngularFirestoreCollection<CuposEstacionamiento>;
  public estacionadoCollection: AngularFirestoreCollection<Estacionado>;

  public cuposEstacionamiento: Observable<CuposEstacionamiento[]>;
  public estacionados: Observable<Estacionado[]>;
  public cupoEstacionamientoDoc: AngularFirestoreDocument<CuposEstacionamiento>;


  constructor(
    public afs: AngularFirestore,
    private store: Store<AppState>,
  ) {
    this.cupoEstacionamientoCollection = this.afs.collection<CuposEstacionamiento>(this.rutaColeccion);
    this.cuposEstacionamiento = this.cupoEstacionamientoCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as CuposEstacionamiento;
        const id = a.payload.doc.id;
        return { id, ...data }
      })));
  }

  obtenerCuposEstacionamiento() {
    return this.cuposEstacionamiento;
  }

  editarCupoEstacionamiento(cuposEstacionamiento: CuposEstacionamiento) {
    return new Promise<any>((resolve, reject) => {
      this.cupoEstacionamientoDoc = this.afs.doc(`${this.rutaColeccion}/${cuposEstacionamiento.idEstacionamiento}`);
      this.cupoEstacionamientoDoc.update(cuposEstacionamiento)
        .then(res => { }, err => reject(err));
    });


    
  }

  agregarEstacionamiento(cuposEstacionamiento: CuposEstacionamiento) {
    var id = Math.random().toString(36).slice(-20)
    cuposEstacionamiento.idEstacionamiento = id;
    return new Promise<any>((resolve, reject) => {
      this.cupoEstacionamientoCollection.doc(id).set(cuposEstacionamiento)
        .then(res => { }, err => reject(err));
    });
  }
}
