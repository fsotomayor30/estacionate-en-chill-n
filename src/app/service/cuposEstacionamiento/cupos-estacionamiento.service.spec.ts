import { TestBed } from '@angular/core/testing';

import { CuposEstacionamientoService } from './cupos-estacionamiento.service';

describe('CuposEstacionamientoService', () => {
  let service: CuposEstacionamientoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CuposEstacionamientoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
