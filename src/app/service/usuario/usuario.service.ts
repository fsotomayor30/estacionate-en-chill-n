import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Usuario } from './../../entities/usuario.entity';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  private rutaColeccion = "Usuarios";

  public usuarioCollection: AngularFirestoreCollection<Usuario>;
  public usuarios: Observable<Usuario[]>;
  public usuarioDoc: AngularFirestoreDocument<Usuario>;

  constructor(public afs: AngularFirestore) {
    this.usuarioCollection = this.afs.collection<Usuario>(this.rutaColeccion);
    this.usuarios = this.usuarioCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Usuario;
        const id = a.payload.doc.id;
        return { id, ...data }
      })));
   }

   obtenerUsuarios(){
    return this.usuarios;
  }

  editarUsuario(usuario: Usuario) {
    this.usuarioDoc = this.afs.doc(`${this.rutaColeccion}/${usuario.id}`);
    this.usuarioDoc.update(usuario);
  }

  agregarUsuario(usuario: Usuario) {
    var id = Math.random().toString(36).slice(-20)
    usuario.id = id;
    return new Promise<any>((resolve, reject) => {
      this.usuarioCollection.doc(id).set(usuario)
        .then(res => { }, err => reject(err));
    });
  }
}
