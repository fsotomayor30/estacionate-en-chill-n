import { UsuarioLogin } from './../entities/usuarioLogin.entity';
import { AppState } from './../app.state';
import { Component, OnInit } from '@angular/core';
import { UtilsService } from 'app/common/utilServices';
import { Store } from '@ngrx/store';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
    rol: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/dashboard', title: 'Dashboard',  icon: 'pe-7s-graph', class: '', rol: 'ESTACIONAMIENTO'},
    { path: '/dashboard', title: 'Dashboard',  icon: 'pe-7s-graph', class: '', rol: 'ADMINISTRADOR' },
    { path: '/mi-estacionamiento', title: 'Mi estacionamiento',  icon:'pe-7s-car', class: '', rol: 'ESTACIONAMIENTO' },
    { path: '/user', title: 'Perfil Usuario',  icon:'pe-7s-user', class: '', rol: 'ADMINISTRADOR' },
    { path: '/user', title: 'Perfil Usuario',  icon:'pe-7s-user', class: '', rol: 'ESTACIONAMIENTO' },
    { path: '/estacionamientos', title: 'Estacionamientos',  icon:'pe-7s-car', class: '' , rol:'ADMINISTRADOR' },
    { path: '/administradores', title: 'Administradores',  icon:'pe-7s-lock', class: '', rol: 'ADMINISTRADOR'},
    { path: '/mapa', title: 'Mapas',  icon:'pe-7s-map-marker', class: '', rol: 'ADMINISTRADOR' },
    { path: '/busquedas', title: 'Búsquedas',  icon:'pe-7s-search', class: '', rol: 'ADMINISTRADOR' },
    { path: '/simulaciones', title: 'Simulaciones',  icon:'pe-7s-graph1', class: '', rol: 'ADMINISTRADOR'  },
    { path: '/usuarios-estacionamientos', title: 'Usuarios',  icon:'pe-7s-key', class: '', rol: 'ADMINISTRADOR' },
    { path: '/table', title: 'Tablas',  icon:'pe-7s-note2', class: '', rol: 'ESTACIONAMIENTO' },
    { path: '/typography', title: 'Typography',  icon:'pe-7s-news-paper', class: '', rol: 'ESTACIONAMIENTO'  },
    { path: '/icons', title: 'Icons',  icon:'pe-7s-science', class: '', rol: 'ESTACIONAMIENTO'  },
    { path: '/notifications', title: 'Notifications',  icon:'pe-7s-bell', class: '',rol: 'ESTACIONAMIENTO'  }
    
  ];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  usuarioLogin: UsuarioLogin;

  constructor(
    private store: Store<AppState>,
  ) {

   }

  ngOnInit() {
    this.store.select('usuarioLogin').subscribe(
      (data:UsuarioLogin)=>{
        if(data!=null){
          this.usuarioLogin=data;
          this.menuItems = ROUTES.filter(menuItem => menuItem);
        }
      }
    )
    

  }

  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };
}
