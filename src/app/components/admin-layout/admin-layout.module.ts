import { AdministradoresComponent } from '../administradores/administradores.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LbdModule } from '../../lbd/lbd.module';
import { NguiMapModule} from '@ngui/map';

import { AdminLayoutRoutes } from './admin-layout.routing';

import { HomeComponent } from '../../home/home.component';
import { UserComponent } from '../user/user.component';
import { TablesComponent } from '../../tables/tables.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { EstacionamientosComponent } from '../estacionamientos/estacionamientos.component'
import { CrearEstacionamientoComponent } from '../estacionamientos/crear-estacionamiento/crear-estacionamiento.component';
import { MatSlideToggleModule} from '@angular/material/slide-toggle';
import { EditarEstacionamientoComponent } from '../estacionamientos/editar-estacionamiento/editar-estacionamiento.component';
import { CrearAdministradorComponent } from '../administradores/crear-administrador/crear-administrador.component';
import { EditarAdministradorComponent } from '../administradores/editar-administrador/editar-administrador.component';
import { BusquedasComponent } from '../busquedas/busquedas.component';
import { SimulacionesComponent } from '../simulaciones/simulaciones.component';
import { CrearUsuarioComponent } from '../usuarios/crear-usuario/crear-usuario.component';
import { EditarUsuarioComponent } from '../usuarios/editar-usuario/editar-usuario.component';
import { UsuariosComponent} from '../usuarios/usuarios.component'
import { CuposEstacionamientoComponent } from '../cupos-estacionamiento/cupos-estacionamiento.component';
import { InicioComponent } from '../inicio/inicio.component';


// import para tabla y filtro
import {DemoMaterialModule} from '../../material-modules';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS} from '@angular/material/form-field';
import {MatNativeDateModule} from '@angular/material/core';

@NgModule({
  imports: [
    MatSlideToggleModule,
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    LbdModule,    
    MatNativeDateModule,
    DemoMaterialModule,
    ReactiveFormsModule,
    NguiMapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=YOUR_KEY_HERE'}),
  ],
  declarations: [
    HomeComponent,
    UserComponent,
    TablesComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    EstacionamientosComponent,
    CrearEstacionamientoComponent,
    EditarEstacionamientoComponent,
    AdministradoresComponent,
    CrearAdministradorComponent,
    EditarAdministradorComponent,
    BusquedasComponent,
    SimulacionesComponent,
    CrearUsuarioComponent,
    EditarUsuarioComponent,
    UsuariosComponent,
    CuposEstacionamientoComponent,
    InicioComponent,


  ],
  providers: [{provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' }
}]
})

export class AdminLayoutModule {}
