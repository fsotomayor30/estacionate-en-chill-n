import { InicioComponent } from './../inicio/inicio.component';
import { EditarUsuarioComponent } from '../usuarios/editar-usuario/editar-usuario.component';
import { CrearUsuarioComponent } from '../usuarios/crear-usuario/crear-usuario.component';
import { UsuariosComponent } from '../usuarios/usuarios.component';
import { SimulacionesComponent } from '../simulaciones/simulaciones.component';
import { BusquedasComponent } from '../busquedas/busquedas.component';
import { EditarAdministradorComponent } from '../administradores/editar-administrador/editar-administrador.component';
import { CrearAdministradorComponent } from '../administradores/crear-administrador/crear-administrador.component';
import { AdministradoresComponent } from '../administradores/administradores.component';
import { EditarEstacionamientoComponent } from '../estacionamientos/editar-estacionamiento/editar-estacionamiento.component';
import { CrearEstacionamientoComponent } from '../estacionamientos/crear-estacionamiento/crear-estacionamiento.component';
import { EstacionamientosComponent } from '../estacionamientos/estacionamientos.component';
import { Routes } from '@angular/router';

import { UserComponent } from '../user/user.component';
import { TablesComponent } from '../../tables/tables.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { CuposEstacionamientoComponent } from '../cupos-estacionamiento/cupos-estacionamiento.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',                    component: InicioComponent},
    { path: 'user',                         component: UserComponent },
    { path: 'table',                        component: TablesComponent },
    { path: 'typography',                   component: TypographyComponent },
    { path: 'icons',                        component: IconsComponent },
    { path: 'mapa',                         component: MapsComponent },
    { path: 'notifications',                component: NotificationsComponent },
    { path: 'estacionamientos',             component: EstacionamientosComponent },
    { path: 'nuevo-estacionamiento',        component: CrearEstacionamientoComponent },
    { path: 'editar-estacionamiento/:id',   component: EditarEstacionamientoComponent },
    { path: 'administradores',              component: AdministradoresComponent },
    { path: 'nuevo-administrador',          component: CrearAdministradorComponent },
    { path: 'editar-administrador/:id',     component: EditarAdministradorComponent },
    { path: 'busquedas',                    component: BusquedasComponent },
    { path: 'simulaciones',                 component: SimulacionesComponent },
    { path: 'usuarios-estacionamientos',    component: UsuariosComponent},
    { path: 'nuevo-usuario',                component: CrearUsuarioComponent},
    { path: 'editar-usuario/:id',           component: EditarUsuarioComponent },
    { path: 'mi-estacionamiento',           component: CuposEstacionamientoComponent },


];
