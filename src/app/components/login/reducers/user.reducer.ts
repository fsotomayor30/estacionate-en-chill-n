import { UsuarioLogin } from '../../../entities/usuarioLogin.entity';
import * as fromUserAction from '../actions/user.action';

const intialStateUser:UsuarioLogin = null;

export function userReducer(state: UsuarioLogin = intialStateUser, action: any): UsuarioLogin {
  switch (action.type) {
    case fromUserAction.UPDATE_USER:
      return _mapUpdateUserActionToState(state, action);
    default:
      return state;
  }
}

function _mapUpdateUserActionToState(state: UsuarioLogin, action: fromUserAction.UpdateUserAction): UsuarioLogin{
  return action.user;
}
