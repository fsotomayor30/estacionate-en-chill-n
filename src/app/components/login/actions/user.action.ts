import { UsuarioLogin } from '../../../entities/usuarioLogin.entity';

export const UPDATE_USER = '[USER] UPDATE_USER';

export class UpdateUserAction {
  type: string = UPDATE_USER;
  user: UsuarioLogin;

  constructor(user: UsuarioLogin) {
    this.user = user;
  }
}

