import { Busqueda } from '../../../entities/busqueda.entity';
import * as fromBusquedaAction from '../actions/busqueda.action';

const intialStateBusqueda:Busqueda[] = [];

export function busquedaReducer(state: Busqueda[] = intialStateBusqueda, action: any): Busqueda[] {
  switch (action.type) {
    case fromBusquedaAction.UPDATE_BUSQUEDA:
      return _mapUpdateBusquedaActionToState(state, action);
    case fromBusquedaAction.ADD_BUSQUEDA:
      return _mapAddBusquedaActionToState(state, action);
    default:
      return state;

  }
}

function _mapUpdateBusquedaActionToState(state: Busqueda[], action: fromBusquedaAction.UpdateBusquedaAction): Busqueda[] {
  return action.busquedas;
}

function _mapAddBusquedaActionToState(state: Busqueda[], action: fromBusquedaAction.AddBusquedaAction): Busqueda[] {
  return [...state, action.busqueda];
}
