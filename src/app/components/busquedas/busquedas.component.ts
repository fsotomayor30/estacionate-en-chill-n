import { Component, OnInit } from '@angular/core';
import { Busqueda} from '../../entities/busqueda.entity'
import { Router } from '@angular/router';
import { AppState } from '../../app.state';
import { Store } from '@ngrx/store';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-busquedas',
  templateUrl: './busquedas.component.html',
  styleUrls: ['./busquedas.component.css']
})
export class BusquedasComponent implements OnInit {

  public busqueda:Busqueda[] = [];
  public displayedColumns: string[]; 
  public dataSource;
  constructor(
    private store: Store<AppState>,
    private router: Router,
  ) {  
    
  }

  ngOnInit(): void {
        //TODO: agregar
      /*     this.store.select('user').subscribe(
      (data: User) => {
        if(data==null) this.router.navigate(['/login']);
      }) */
      
    //TODO: Terminar
    this.store.select('busquedas').subscribe((data:Busqueda[])=>{
      this.busqueda=data;   
     this.displayedColumns = ['correo', 'fecha', 'nombreLugar'];
     this.dataSource= new MatTableDataSource(this.busqueda);      
    });
  
     
   
  }
 
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();    
  }
  
}
