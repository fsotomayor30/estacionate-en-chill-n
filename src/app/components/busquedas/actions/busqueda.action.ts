import { Busqueda } from '../../../entities/busqueda.entity';

export const UPDATE_BUSQUEDA = '[BUSQUEDA] UPDATE_ADMINISTRADOR';
export const ADD_BUSQUEDA = '[BUSQUEDA] ADD_ADMINISTRADOR';

export class UpdateBusquedaAction {
  type: string = UPDATE_BUSQUEDA;
  busquedas: Busqueda[];

  constructor(busquedas: Busqueda[]) {
    this.busquedas = busquedas;
  }
}

export class AddBusquedaAction {
  type: string = ADD_BUSQUEDA;
  busqueda: Busqueda;

  constructor(busqueda: Busqueda) {
    this.busqueda = busqueda;
  }
}


