import { CuposEstacionamientoService } from './../../service/cuposEstacionamiento/cupos-estacionamiento.service';
import { ThemePalette } from '@angular/material/core';
import { EstacionamientoService } from './../../service/estacionamiento/estacionamiento.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Usuario } from './../../entities/usuario.entity';
import { Estacionamiento } from './../../entities/estacionamiento.entity';
import { Router } from '@angular/router';
import { AppState } from '../../app.state';
import { UsuarioLogin } from '../../entities/usuarioLogin.entity';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { UtilsService } from 'app/common/utilServices';
import { CuposEstacionamiento } from 'app/entities/cuposEstacionamiento.entity';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  public user: UsuarioLogin;
  public estacionamiento: Estacionamiento;
  public usuario: Usuario;
  public cupoEstacionamiento:CuposEstacionamiento;
  public validCupos;
  //CONFIGURACION DEL FORMULARIO
  form: FormGroup
  formCupos: FormGroup

    //CONFIGURACION DEL TOOGLE
    color: ThemePalette = 'primary';

  constructor(
    private store: Store<AppState>,
    private router: Router,
    private formBuilder: FormBuilder,
    private estacionamientoService: EstacionamientoService,
    private cuposEstacionamientoService: CuposEstacionamientoService,
    private utils:UtilsService
    ) {
      this.buildForm();
      this.buildFormCupos();
     }

     private buildFormCupos(){
      this.formCupos = this.formBuilder.group({
        idEstacionamiento: ['',[Validators.required]],
        cuposTotales: ['', [Validators.required]]
      })
     }

     private buildForm() {
      this.form = this.formBuilder.group({
        id: ['', [Validators.required]],
        nombre: ['', [Validators.required]],
        direccion: ['', [Validators.required]],
        latitud: ['', [Validators.required]],
        longitud: ['', [Validators.required]],
        telefono: ['', [Validators.required]],
        esTechado: ['', [Validators.required]],
        precioPorMinuto: ['', [Validators.required]],
        tieneCamaraSeguridad: ['', [Validators.required]],
        habilitado: ['', [Validators.required]],
      })
    }

  ngOnInit() {
    this.store.select('usuarioLogin').subscribe(
      (data: UsuarioLogin) => {
        if(data!=null){
          this.user = data;
          if(data.rol=="ESTACIONAMIENTO"){
            this.store.select('usuarios').subscribe(
              (data: Usuario[]) => {
                this.usuario = data.find((usuarioFind) => usuarioFind.email == this.user.email);
                 if(this.usuario!= null){
                  this.store.select('estacionamientos').subscribe(
                    (data: Estacionamiento[]) =>{
                      this.estacionamiento = data.find(estacionamiento => estacionamiento.id==this.usuario.estacionamiento);
                      console.log("ESTACIONAMIENTO => "+ this.estacionamiento.id);
                      this.form.setValue(this.estacionamiento);
                    }
                  );
                  this.store.select('cuposEstacionamientos').subscribe(
                    (data: CuposEstacionamiento[]) => {
                      this.cupoEstacionamiento=data.find(cupoEstacionamientoFind => this.estacionamiento.id === cupoEstacionamientoFind.idEstacionamiento);
                      if(this.cupoEstacionamiento == null){
                        this.validCupos=false;
                      }else{
                        this.validCupos=true;
                        this.formCupos.setValue({
                          idEstacionamiento: this.cupoEstacionamiento.idEstacionamiento,
                          cuposTotales: this.cupoEstacionamiento.cuposTotales,
                        });
                      }
                    }
                  );
                } 
              }
            );
          }
        }else{
          this.router.navigate(['/login']);
        }
        
      }
    )
  }

  guardar(event: Event) {
    event.preventDefault();
    if (this.form.valid) {
      const value = this.form.value;
      this.estacionamientoService.editarEstacionamiento(value);
      this.form.reset;
      this.utils.showNotificationExito('top', 'center', "<b>Éxito</b> estacionamiento actualizado.");
    } else {
      this.form.markAllAsTouched();
    }
  }

  guardarCupos(event: Event) {
    event.preventDefault();
    if (this.formCupos.valid) {
      const value = this.formCupos.value;
      this.cuposEstacionamientoService.editarCupoEstacionamiento(value);
      this.utils.showNotificationExito('top', 'center', "<b>Éxito</b> cupos actualizados.");
    } else {
      this.formCupos.markAllAsTouched();
    }
  }

}
