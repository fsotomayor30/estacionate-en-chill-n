import { Estacionamiento } from '../../../entities/estacionamiento.entity';
import * as fromEstacionamientoAction from '../actions/estacionamiento.action';

const intialStateEstacionamiento:Estacionamiento[] = [];

export function estacionamientoReducer(state: Estacionamiento[] = intialStateEstacionamiento, action: any): Estacionamiento[] {
  switch (action.type) {
    case fromEstacionamientoAction.UPDATE_ESTACIONAMIENTO:
      return _mapUpdateEstacionamientoActionToState(state, action);
    case fromEstacionamientoAction.ADD_ESTACIONAMIENTO:
      return _mapAddEstacionamientoActionToState(state, action);
    default:
      return state;

  }
}

function _mapUpdateEstacionamientoActionToState(state: Estacionamiento[], action: fromEstacionamientoAction.UpdateEstacionamientoAction): Estacionamiento[] {
  return action.estacionamientos;
}

function _mapAddEstacionamientoActionToState(state: Estacionamiento[], action: fromEstacionamientoAction.AddEstacionamientoAction): Estacionamiento[] {
  return [...state, action.estacionaminto];
}
