import { Router } from '@angular/router';
import { AppState } from '../../app.state';
import { Estacionamiento } from '../../entities/estacionamiento.entity';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';


@Component({
  selector: 'app-estacionamientos',
  templateUrl: './estacionamientos.component.html',
  styleUrls: ['./estacionamientos.component.css']
})
export class EstacionamientosComponent implements OnInit {

  estacionamientos: Estacionamiento[] = [];

  constructor(    
    private store: Store<AppState>,
    private router: Router,
    ) { }

  ngOnInit() {
    //TODO: agregar
      /*     this.store.select('user').subscribe(
      (data: User) => {
        if(data==null) this.router.navigate(['/login']);
      }) */
    
    this.store.select('estacionamientos').subscribe(
      (data: Estacionamiento[]) => {
          this.estacionamientos=data;
      })
  }

  

  editar(id:string){
    this.router.navigate(['/editar-estacionamiento', id])
  }
}
