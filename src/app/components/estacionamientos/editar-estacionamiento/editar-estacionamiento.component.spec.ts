import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarEstacionamientoComponent } from './editar-estacionamiento.component';

describe('EditarEstacionamientoComponent', () => {
  let component: EditarEstacionamientoComponent;
  let fixture: ComponentFixture<EditarEstacionamientoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarEstacionamientoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarEstacionamientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
