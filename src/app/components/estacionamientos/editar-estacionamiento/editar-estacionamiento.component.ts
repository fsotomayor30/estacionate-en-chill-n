import { ThemePalette } from '@angular/material/core';
import { EstacionamientoService } from '../../../service/estacionamiento/estacionamiento.service';
import { Estacionamiento } from '../../../entities/estacionamiento.entity';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AppState } from '../../../app.state';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { UtilsService } from 'app/common/utilServices';

@Component({
  selector: 'app-editar-estacionamiento',
  templateUrl: './editar-estacionamiento.component.html',
  styleUrls: ['./editar-estacionamiento.component.css']
})
export class EditarEstacionamientoComponent implements OnInit {

  //CONFIGURACION DEL FORMULARIO
  form: FormGroup

  //CONFIGURACION DEL TOOGLE
  color: ThemePalette = 'primary';

  private estacionamientos: Estacionamiento[] = [];
  private estacionamiento: Estacionamiento;
  private idValido: boolean;
  private id: string;
  constructor(
    private route: ActivatedRoute,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private estacionamientoService: EstacionamientoService,
    private utils:UtilsService
  ) {
    this.buildForm();

  }

  ngOnInit(): void {
        //TODO: agregar
      /*     this.store.select('user').subscribe(
      (data: User) => {
        if(data==null) this.router.navigate(['/login']);
      }) */
      
    this.route.paramMap.subscribe(params => {
      if (params.has("id")) {
        console.log(params.get("id"));
        this.id = params.get("id");
        this.idValido = true;
        this.store.select("estacionamientos").subscribe(
          (data: Estacionamiento[]) => {
            this.estacionamientos = data
          }
        )
        this.estacionamiento = this.estacionamientos.find(e => e.id == this.id);
        if (this.estacionamiento == undefined) {
          this.idValido = false;
          this.desactivarForm();
        } else {
          console.log(this.estacionamiento);
          this.form.setValue(this.estacionamiento);
        }
      }
    })
  }

  private buildForm() {
    this.form = this.formBuilder.group({
      id: ['', [Validators.required]],
      nombre: ['', [Validators.required]],
      direccion: ['', [Validators.required]],
      latitud: ['', [Validators.required]],
      longitud: ['', [Validators.required]],
      telefono: ['', [Validators.required]],
      esTechado: ['', [Validators.required]],
      precioPorMinuto: ['', [Validators.required]],
      tieneCamaraSeguridad: ['', [Validators.required]],
      habilitado: ['', [Validators.required]],

    })
  }

  private desactivarForm(){
    this.form.controls['nombre'].disable();
    this.form.controls['direccion'].disable();
    this.form.controls['latitud'].disable();
    this.form.controls['longitud'].disable();
    this.form.controls['telefono'].disable();
    this.form.controls['esTechado'].disable();
    this.form.controls['precioPorMinuto'].disable();
    this.form.controls['tieneCamaraSeguridad'].disable();
    this.form.controls['habilitado'].disable();

  }

  guardar(event: Event) {
    event.preventDefault();
    if (this.form.valid) {
      const value = this.form.value;
      this.estacionamientoService.editarEstacionamiento(value);
      this.form.reset;
      this.utils.showNotificationExito('top', 'center', "<b>Éxito</b> estacionamiento actualizado.");
    } else {
      this.form.markAllAsTouched();
    }
  }
}
