import { UsuarioLogin } from './../../../entities/usuarioLogin.entity';
import { AppState } from '../../../app.state';
import { Router } from '@angular/router';
import { EstacionamientoService } from '../../../service/estacionamiento/estacionamiento.service';
import { Component, OnInit } from '@angular/core';
import { ThemePalette } from '@angular/material/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Store } from '@ngrx/store';
import { UtilsService } from 'app/common/utilServices';


@Component({
  selector: 'app-crear-estacionamiento',
  templateUrl: './crear-estacionamiento.component.html',
  styleUrls: ['./crear-estacionamiento.component.css']
})
export class CrearEstacionamientoComponent implements OnInit {

  //CONFIGURACION DEL FORMULARIO
  form: FormGroup

  //CONFIGURACION DEL TOOGLE
  color: ThemePalette = 'primary';

  constructor(
    private formBuilder: FormBuilder,
    private estacionamientoService: EstacionamientoService,
    private store: Store<AppState>,
    private router: Router,
    private utils:UtilsService

  ) {
    this.buildForm();
  }

  ngOnInit(): void {


    this.store.select('usuarioLogin').subscribe(
      (data: UsuarioLogin) => {
        if(data==null) this.router.navigate(['/login']);
      })
  }

  private buildForm() {
    this.form = this.formBuilder.group({
      nombre: ['', [Validators.required]],
      direccion: ['', [Validators.required]],
      latitud: ['', [Validators.required]],
      longitud: ['', [Validators.required]],
      telefono: ['', [Validators.required]],
      esTechado: ['', [Validators.required]],
      precioPorMinuto: ['', [Validators.required]],
      tieneCamaraSeguridad: ['', [Validators.required]],
      habilitado: ['', [Validators.required]],
    })
  }

  guardar(event: Event) {
    event.preventDefault();
    if(this.form.valid){
      const value = this.form.value;
      this.estacionamientoService.agregarEstacionamiento(value);
      this.form.reset();
      this.utils.showNotificationExito('top', 'center',"<b>Éxito</b> estacionamiento agregado.");
    }else{
      this.form.markAllAsTouched();
    }
  }
}
