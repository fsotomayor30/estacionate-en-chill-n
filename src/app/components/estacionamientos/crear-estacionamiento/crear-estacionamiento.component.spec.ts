import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearEstacionamientoComponent } from './crear-estacionamiento.component';

describe('CrearEstacionamientoComponent', () => {
  let component: CrearEstacionamientoComponent;
  let fixture: ComponentFixture<CrearEstacionamientoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearEstacionamientoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearEstacionamientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
