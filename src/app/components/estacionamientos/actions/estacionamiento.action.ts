import { Estacionamiento } from '../../../entities/estacionamiento.entity';

export const UPDATE_ESTACIONAMIENTO = '[ESTACIONAMIENTO] UPDATE_ESTACIONAMIENTO';
export const ADD_ESTACIONAMIENTO = '[ESTACIONAMIENTO] ADD_ESTACIONAMIENTO';

export class UpdateEstacionamientoAction {
  type: string = UPDATE_ESTACIONAMIENTO;
  estacionamientos: Estacionamiento[];

  constructor(estacionamientos: Estacionamiento[]) {
    this.estacionamientos = estacionamientos;
  }
}

export class AddEstacionamientoAction {
  type: string = ADD_ESTACIONAMIENTO;
  estacionaminto: Estacionamiento;

  constructor(estacionamiento: Estacionamiento) {
    this.estacionaminto = estacionamiento;
  }
}


