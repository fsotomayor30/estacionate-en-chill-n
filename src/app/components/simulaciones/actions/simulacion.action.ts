import { Simulacion } from '../../../entities/simulacion.entity';

export const UPDATE_SIMULACION = '[SIMULACION] UPDATE_ADMINISTRADOR';
export const ADD_SIMULACION = '[SIMULACION] ADD_ADMINISTRADOR';

export class UpdateSimulacionAction {
  type: string = UPDATE_SIMULACION;
  simulaciones: Simulacion[];

  constructor(simulacion: Simulacion[]) {
    this.simulaciones = simulacion;
  }
}

export class AddSimulacionAction {
  type: string = ADD_SIMULACION;
  simulaciones: Simulacion;

  constructor(simulacion: Simulacion) {
    this.simulaciones = simulacion;
  }
}


