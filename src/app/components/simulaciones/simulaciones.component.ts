import { map } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import{Simulacion} from '../../entities/simulacion.entity'
import { Router } from '@angular/router';
import { AppState } from '../../app.state';
import {MatTableDataSource} from '@angular/material/table';
import { Store } from '@ngrx/store';
import { Estacionamiento } from '../../entities/estacionamiento.entity';
import { UtilsService } from 'app/common/utilServices';

@Component({
  selector: 'app-simulaciones',
  templateUrl: './simulaciones.component.html',
  styleUrls: ['./simulaciones.component.css']
})
export class SimulacionesComponent implements OnInit {

  public simulaciones:Simulacion[] = [];
  public estacionamientos:Estacionamiento[]=[];
  public displayedColumns: string[]=[]; 
  public dataSource;
  public totalSimulacion:number=0;
  constructor(
    private store: Store<AppState>,
    private router: Router,
    private utils: UtilsService
  ) { }
 

  ngOnInit(): void {
        //TODO: agregar
      /*     this.store.select('user').subscribe(
      (data: User) => {
        if(data==null) this.router.navigate(['/login']);
      }) */
      
        this.store.select('estacionamientos').subscribe(
          (data:Estacionamiento[])=>{
          this.estacionamientos=data;
        });

        this.store.select('simulaciones').subscribe((data:Simulacion[])=>{
          this.displayedColumns = ['estacionamientoId', 'fecha', 'minutos','precio','total'];
          this.dataSource= new MatTableDataSource(this.simulacionesNombreEstacionamiento(data));   
          this.calcularTotalSimulacion(this.simulacionesNombreEstacionamiento(data));
        });
  }

  simulacionesNombreEstacionamiento(simulaciones: Simulacion[]):Simulacion[]{
    let simulacionesReturn = simulaciones.map((simulacion) => {
      let simulacionReturn:Simulacion = {
        id: simulacion.id,
        fecha: simulacion.fecha,
        minutos: simulacion.minutos,
        precio: simulacion.precio,
        total: simulacion.total,
        estacionamientoId: this.utils.buscarEstacionamientoPorId(simulacion.estacionamientoId, this.estacionamientos).nombre
      }
      return simulacionReturn;
    })
    return simulacionesReturn;
  }

  calcularTotalSimulacion(data){
    this.totalSimulacion=0;
    data.forEach(element => {
      this.totalSimulacion = element.total + this.totalSimulacion;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();   
    this.calcularTotalSimulacion(this.dataSource.filteredData);
  }
}
