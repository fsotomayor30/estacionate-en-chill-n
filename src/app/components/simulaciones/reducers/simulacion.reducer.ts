import { Simulacion } from '../../../entities/simulacion.entity';
import * as fromSimulacionAction from '../actions/simulacion.action';

const intialStateSimulacion:Simulacion[] = [];

export function simulacionReducer(state: Simulacion[] = intialStateSimulacion, action: any): Simulacion[] {
  switch (action.type) {
    case fromSimulacionAction.UPDATE_SIMULACION:
      return _mapUpdateSimulacionActionToState(state, action);
    case fromSimulacionAction.ADD_SIMULACION:
      return _mapAddSimulacionActionToState(state, action);
    default:
      return state;

  }
}

function _mapUpdateSimulacionActionToState(state: Simulacion[], action: fromSimulacionAction.UpdateSimulacionAction): Simulacion[] {
  return action.simulaciones;
}

function _mapAddSimulacionActionToState(state: Simulacion[], action: fromSimulacionAction.AddSimulacionAction): Simulacion[] {
  return [...state, action.simulaciones];
}
