import { Estacionamiento } from '../../../entities/estacionamiento.entity';
import { UsuarioLogin } from '../../../entities/usuarioLogin.entity';
import { UsuarioService } from '../../../service/usuario/usuario.service';
import { Router } from '@angular/router';
import { AppState } from '../../../app.state';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ThemePalette } from '@angular/material/core';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { UtilsService } from 'app/common/utilServices';


@Component({
  selector: 'app-crear-usuario',
  templateUrl: './crear-usuario.component.html',
  styleUrls: ['./crear-usuario.component.css']
})
export class CrearUsuarioComponent implements OnInit {

  estacionamientos: Estacionamiento[]=[]
  //CONFIGURACION DEL FORMULARIO
  form: FormGroup

  //CONFIGURACION DEL TOOGLE
  color: ThemePalette = 'primary';

  constructor(
    private formBuilder: FormBuilder,
    private usuarioService: UsuarioService,
    private store: Store<AppState>,
    private router: Router,
    private utils:UtilsService

  ) {
    this.buildForm();

   }

  ngOnInit(): void {
    //TODO: TERMINAR
/*     this.store.select('user').subscribe(
      (data: User) => {
        if(data==null) this.router.navigate(['/login']);
      }) */

      this.store.select('estacionamientos').subscribe(
        (data:Estacionamiento[])=> {
          this.estacionamientos=data;
        }
      )
  }

  private buildForm() {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      estacionamiento: ['', [Validators.required]],
      habilitado: ['', [Validators.required]],
    })
  }

  guardar(event: Event) {
    event.preventDefault();
    if(this.form.valid){
      const value = this.form.value;
      this.usuarioService.agregarUsuario(value);
      this.form.reset();
      this.utils.showNotificationExito('top', 'center', "<b>Éxito</b> usuario agregado.");
    }else{
      this.form.markAllAsTouched();
    }
  }


}
