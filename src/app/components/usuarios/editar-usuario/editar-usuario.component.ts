import { Estacionamiento } from './../../../entities/estacionamiento.entity';
import { UsuarioService } from './../../../service/usuario/usuario.service';
import { Usuario } from './../../../entities/usuario.entity';
import { AppState } from './../../../app.state';
import { ActivatedRoute } from '@angular/router';
import { ThemePalette } from '@angular/material/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { UtilsService } from 'app/common/utilServices';


@Component({
  selector: 'app-editar-usuario',
  templateUrl: './editar-usuario.component.html',
  styleUrls: ['./editar-usuario.component.css']
})
export class EditarUsuarioComponent implements OnInit {

    estacionamientos: Estacionamiento[]=[]

    //CONFIGURACION DEL FORMULARIO
    form: FormGroup

    //CONFIGURACION DEL TOOGLE
    color: ThemePalette = 'primary';

    private usuarios: Usuario[] = [];
    private usuario: Usuario;
    private id: string;
    private idValido: boolean;

  constructor(
    private route: ActivatedRoute,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private usuarioService: UsuarioService,
    private utils:UtilsService
  ) {
    this.buildForm();
   }

  ngOnInit(): void {
      //TODO: agregar
      /*     this.store.select('user').subscribe(
      (data: User) => {
        if(data==null) this.router.navigate(['/login']);
      }) */

      this.route.paramMap.subscribe(params => {
        if (params.has("id")) {
          this.store.select('estacionamientos').subscribe(
            (data:Estacionamiento[])=> {
              this.estacionamientos=data;
            }
          )

          this.id = params.get("id");
          this.idValido = true;
          this.store.select("usuarios").subscribe(
            (data: Usuario[]) => {
              this.usuarios = data
            }
          )
          this.usuario = this.usuarios.find(e => e.id == this.id);
          if (this.usuario == undefined) {
            this.idValido = false;
            this.desactivarForm();
          } else {
            this.form.setValue(this.usuario);
          }
        }
      })
  }

  private buildForm() {
    this.form = this.formBuilder.group({
      id: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      estacionamiento: ['', [Validators.required]],
      habilitado: ['', [Validators.required]],
    })
  }

  private desactivarForm(){
    this.form.controls['email'].disable();
    this.form.controls['estacionamiento'].disable();
    this.form.controls['habilitado'].disable();
  }

  guardar(event: Event) {
    event.preventDefault();
    if (this.form.valid) {
      const value = this.form.value;
      this.usuarioService.editarUsuario(value);
      this.form.reset;
      this.utils.showNotificationExito('top', 'center', "<b>Éxito</b> usuario actualizado.");
    } else {
      this.form.markAllAsTouched();
    }
  }
}
