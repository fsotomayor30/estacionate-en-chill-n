import { Usuario } from '../../../entities/usuario.entity';

export const UPDATE_USUARIO = '[USUARIO] UPDATE_USUARIO';
export const ADD_USUARIO = '[USUARIO] ADD_USUARIO';

export class UpdateUsuarioAction {
  type: string = UPDATE_USUARIO;
  usuarios: Usuario[];

  constructor(usuario: Usuario[]) {
    this.usuarios = usuario;
  }
}

export class AddUsuarioAction {
  type: string = ADD_USUARIO;
  usuarios: Usuario;

  constructor(usuario: Usuario) {
    this.usuarios = usuario;
  }
}


