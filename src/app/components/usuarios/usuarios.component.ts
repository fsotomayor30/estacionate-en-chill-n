import { Estacionamiento } from './../../entities/estacionamiento.entity';
import { AppState } from '../../app.state';
import { Router } from '@angular/router';
import { Usuario } from '../../entities/usuario.entity';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { UtilsService } from 'app/common/utilServices';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

  usuarios: Usuario[] = [];
  estacionamientos:Estacionamiento[]=[];

  constructor(
    private store: Store<AppState>,
    private router: Router,
    private utils: UtilsService
  ) { }

  ngOnInit(): void {
        //TODO: agregar
      /*     this.store.select('user').subscribe(
      (data: User) => {
        if(data==null) this.router.navigate(['/login']);
      }) */

      this.store.select('usuarios').subscribe(
        (data: Usuario[]) => {
            this.usuarios=data;
        })

        this.store.select('estacionamientos').subscribe((data:Estacionamiento[])=>{
          this.estacionamientos=data;
        });
  }

  editar(id:string){
    this.router.navigate(['/editar-usuario', id])
  }

  conversionIdANombre(idEstacionamiento:String) : Estacionamiento{
    return this.utils.buscarEstacionamientoPorId(idEstacionamiento, this.estacionamientos);
  }

}
