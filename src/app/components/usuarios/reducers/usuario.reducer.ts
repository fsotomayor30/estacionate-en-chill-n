import { Usuario } from '../../../entities/usuario.entity';
import * as fromUsuarioAction from '../actions/usuario.action';

const intialStateUsuario:Usuario[] = [];

export function userReducer(state: Usuario[] = intialStateUsuario, action: any): Usuario[] {
  switch (action.type) {
    case fromUsuarioAction.UPDATE_USUARIO:
      return _mapUpdateUsuarioActionToState(state, action);
    case fromUsuarioAction.ADD_USUARIO:
      return _mapAddUsuarioActionToState(state, action);
    default:
      return state;

  }
}

function _mapUpdateUsuarioActionToState(state: Usuario[], action: fromUsuarioAction.UpdateUsuarioAction): Usuario[] {
  return action.usuarios;
}

function _mapAddUsuarioActionToState(state: Usuario[], action: fromUsuarioAction.AddUsuarioAction): Usuario[] {
  return [...state, action.usuarios];
}
