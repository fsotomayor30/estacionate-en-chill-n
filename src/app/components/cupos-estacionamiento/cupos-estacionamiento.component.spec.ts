import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CuposEstacionamientoComponent } from './cupos-estacionamiento.component';

describe('CuposEstacionamientoComponent', () => {
  let component: CuposEstacionamientoComponent;
  let fixture: ComponentFixture<CuposEstacionamientoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CuposEstacionamientoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CuposEstacionamientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
