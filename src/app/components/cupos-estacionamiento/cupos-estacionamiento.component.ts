import { Component, OnInit } from '@angular/core';
import { AppState } from 'app/app.state';
import { Router } from '@angular/router';
import { UtilsService } from 'app/common/utilServices';
import { Store } from '@ngrx/store';
import { CuposEstacionamientoService } from 'app/service/cuposEstacionamiento/cupos-estacionamiento.service';
import { EstacionadoService } from 'app/service/estacionado/estacionado.service';
import { Estacionado } from 'app/entities/estacionado.entity';
import { Estacionamiento } from './../../entities/estacionamiento.entity';
import { CuposEstacionamiento } from 'app/entities/cuposEstacionamiento.entity';
import { UpdateCupoEstacionamientoAction } from './actions/cuposEstacionamiento.action';
import * as moment from 'moment';


@Component({
  selector: 'app-cupos-estacionamiento',
  templateUrl: './cupos-estacionamiento.component.html',
  styleUrls: ['./cupos-estacionamiento.component.css']
})
export class CuposEstacionamientoComponent implements OnInit {
  public miEstacionamiento: Estacionamiento;
  public autoEstacionado:Estacionado={id: "",patenteVehiculo: "",fechaLlegada: "",fechaRetirada: "",estacionado: false,idEstacionamiento: "",pagoEstacionamineto:0};
  public autoEstacionadoSaliendo:Estacionado={id: "",patenteVehiculo: "",fechaLlegada: "",fechaRetirada: "",estacionado: false,idEstacionamiento: "",pagoEstacionamineto:0};
  public cuposEstacionamiento: CuposEstacionamiento ={idEstacionamiento:"",cuposTotales:0,cuposDisponibles:0};
  public estacionamientosOcupados:Estacionado[]=[];
  public estacionamientosDesocupados:Estacionado[]=[];
  public estacionadosActivos:Estacionado[]=[];

  public idEstacionamiento:string;
  public estacionadosSuscribe:Estacionado[]=[];

  constructor(
    private store: Store<AppState>,
    private router: Router,
    private utils: UtilsService,
    private cuposEstacionamientoService:CuposEstacionamientoService,
    private estacionadoService:EstacionadoService,
    
  ) { }

  ngOnInit(): void {
    this.idEstacionamiento= this.utils.returnIdLogueado();
    this.mostrarOcupados();
    this.mostrarDesocupados();
  }

  // Muestra y actuiza los Autos que ya estan estacionados
  mostrarDesocupados(){
    this.store.select("cuposEstacionamientos").subscribe(
      (data:CuposEstacionamiento[]) =>{
        this.cuposEstacionamiento = data.find(parkinCupos=> parkinCupos.idEstacionamiento==this.idEstacionamiento);
        this.estacionamientosDesocupados=[];
        let EDisponibles:Estacionado;
        let j:number =0;
        for(j;j<this.cuposEstacionamiento.cuposDisponibles;j++){
         EDisponibles={
           id:  "",
           patenteVehiculo: "",
           fechaLlegada: "",
           fechaRetirada: "",
           estacionado: false,
           idEstacionamiento: this.idEstacionamiento,
           pagoEstacionamineto:0
         }
         this.estacionamientosDesocupados.push(EDisponibles);
       }
      }
    );
  }
  //Muestra y actualiza los cupos disponibles
  mostrarOcupados(){
    this.store.select('estacionados').subscribe((data:Estacionado[])=>{
      this.estacionadosSuscribe = data.filter(autoEstacionado=>autoEstacionado.idEstacionamiento == this.idEstacionamiento && autoEstacionado.estacionado);
      this.estacionamientosOcupados=[];
      let i:number =0;
      let EOcupados:Estacionado;
      this.estacionadosSuscribe.forEach(est => {
        EOcupados ={
          id: est.id,
          patenteVehiculo: est.patenteVehiculo,
          fechaLlegada: est.fechaLlegada,
          fechaRetirada: est.fechaRetirada,
          estacionado: est.estacionado,
          idEstacionamiento: est.idEstacionamiento,
          pagoEstacionamineto:0
        }
        this.estacionamientosOcupados.push(EOcupados);
      });
    });
  }

  clickOcupados(datosEstacionado){
    let miEstacionamiento:Estacionamiento = this.utils.returnEstacionamientoxID(this.idEstacionamiento);
    console.log("mi etacionamiento =>", miEstacionamiento);
    moment.locale('es');
    let fechaActual = moment().format('l LTS');  
    let diferenciaFechas = moment(fechaActual).diff(moment(datosEstacionado.fechaLlegada),'minute');
    let totalAPagar:number = diferenciaFechas * miEstacionamiento.precioPorMinuto;
    console.log("minutos de diferencia =>",diferenciaFechas);
    this.autoEstacionadoSaliendo={
      id: datosEstacionado.id,
      patenteVehiculo: datosEstacionado.patenteVehiculo,
      fechaLlegada:datosEstacionado.fechaLlegada,
      fechaRetirada:fechaActual,
      estacionado:datosEstacionado.estacionado,
      idEstacionamiento:datosEstacionado.idEstacionamiento,
      pagoEstacionamineto:totalAPagar
    }
  }

  clickDesocupado(datos){
    moment.locale('es');
    let fechaActual = moment().format('l LTS');
    this.autoEstacionado ={
    id: datos.id,
    patenteVehiculo: datos.patenteVehiculo,
    fechaLlegada: fechaActual,
    fechaRetirada: datos.fechaRetirada,
    estacionado: datos.estacionado,
    idEstacionamiento: datos.idEstacionamiento,
    pagoEstacionamineto:0
    };
  }

  agregarAuto(){
    this.autoEstacionado.estacionado = true;
    this.estacionadoService.agregarEstacionado(this.autoEstacionado);
    this.actualizarCupos(-1);
    $('#AgregarVehiculo').hide();
  }
  quitarAuto(){
    this.autoEstacionadoSaliendo.estacionado = false;
    this.estacionadoService.editarEstacionado(this.autoEstacionadoSaliendo);
    this.actualizarCupos(1);
    $('#SalidaVehiculo').hide();
  }

  
  

   actualizarCupos(cupo:number){
    let cuposEstacionamiento = this.utils.returnCuposEstacionamientoxID(this.idEstacionamiento);
    let ActualizarCupos:number = cuposEstacionamiento.cuposDisponibles + cupo;
    let CuposActualizados:CuposEstacionamiento={
      idEstacionamiento:cuposEstacionamiento.idEstacionamiento,
      cuposTotales:cuposEstacionamiento.cuposTotales,
      cuposDisponibles: ActualizarCupos 
    };
    this.cuposEstacionamientoService.editarCupoEstacionamiento(CuposActualizados);
   }

   


  
}

