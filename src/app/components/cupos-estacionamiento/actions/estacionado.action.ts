import { Estacionado } from '../../../entities/estacionado.entity';

export const UPDATE_ESTACIONADO = '[ESTACIONADO] UPDATE_ESTACIONADO';
export const ADD_ESTACIONADO = '[ESTACIONADO] ADD_ESTACIONADO';

export class UpdateEstacionadoAction {
  type: string = UPDATE_ESTACIONADO;
  estacionados: Estacionado[];

  constructor(estacionados: Estacionado[]) {
    this.estacionados = estacionados;
  }
}

export class AddEstacionadoAction {
  type: string = ADD_ESTACIONADO;
  estacionado: Estacionado;

  constructor(estacionado: Estacionado) {
    this.estacionado = estacionado;
  }
}


