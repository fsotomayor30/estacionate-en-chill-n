import {CuposEstacionamiento  } from '../../../entities/cuposEstacionamiento.entity';

export const UPDATE_CUPO_ESTACIONAMIENTO = '[CUPO_ESTACIONAMIENTO] UPDATE_CUPO_ESTACIONAMIENTO';
export const ADD_CUPO_ESTACIONAMIENTO = '[CUPO_ESTACIONAMIENTO] ADD_CUPO_ESTACIONAMIENTO';

export class UpdateCupoEstacionamientoAction {
  type: string = UPDATE_CUPO_ESTACIONAMIENTO;
  cuposEstacionamiento: CuposEstacionamiento[];

  constructor(cuposEstacionamiento: CuposEstacionamiento[]) {
    this.cuposEstacionamiento = cuposEstacionamiento;
  }
}

export class AddCupoEstacionamientoAction {
  type: string = ADD_CUPO_ESTACIONAMIENTO;
  cuposEstacionamiento: CuposEstacionamiento;

  constructor(cuposEstacionamiento: CuposEstacionamiento) {
    this.cuposEstacionamiento = cuposEstacionamiento;
  }
}


