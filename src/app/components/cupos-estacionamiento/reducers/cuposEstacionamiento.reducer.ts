import { CuposEstacionamiento } from './../../../entities/cuposEstacionamiento.entity';
import * as fromCupoEstacionamientoAction from '../actions/cuposEstacionamiento.action';

const intialStateCupoEstacionamiento:CuposEstacionamiento[] = [];

export function estacionamientoReducer(state: CuposEstacionamiento[] = intialStateCupoEstacionamiento, action: any): CuposEstacionamiento[] {
  switch (action.type) {
    case fromCupoEstacionamientoAction.UPDATE_CUPO_ESTACIONAMIENTO:
      return _mapUpdateCupoEstacionamientoActionToState(state, action);
    case fromCupoEstacionamientoAction.ADD_CUPO_ESTACIONAMIENTO:
      return _mapAddCupoEstacionamientoActionToState(state, action);
    default:
      return state;

  }
}

function _mapUpdateCupoEstacionamientoActionToState(state: CuposEstacionamiento[], action: fromCupoEstacionamientoAction.UpdateCupoEstacionamientoAction): CuposEstacionamiento[] {
  return action.cuposEstacionamiento;
}

function _mapAddCupoEstacionamientoActionToState(state: CuposEstacionamiento[], action: fromCupoEstacionamientoAction.AddCupoEstacionamientoAction): CuposEstacionamiento[] {
  return [...state, action.cuposEstacionamiento];
}
