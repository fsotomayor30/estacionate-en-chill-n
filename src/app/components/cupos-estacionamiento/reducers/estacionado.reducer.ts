import * as fromEstacionadoAction from '../actions/estacionado.action';
import { Estacionado } from 'app/entities/estacionado.entity';

const intialStateEstacionado:Estacionado[] = [];

export function estacionadoReducer(state: Estacionado[] = intialStateEstacionado, action: any): Estacionado[] {
  switch (action.type) {
    case fromEstacionadoAction.UPDATE_ESTACIONADO:
      return _mapUpdateEstacionadoActionToState(state, action);
    case fromEstacionadoAction.ADD_ESTACIONADO:
      return _mapAddEstacionadoActionToState(state, action);
    default:
      return state;

  }
}

function _mapUpdateEstacionadoActionToState(state: Estacionado[], action: fromEstacionadoAction.UpdateEstacionadoAction): Estacionado[] {
  return action.estacionados;
}

function _mapAddEstacionadoActionToState(state: Estacionado[], action: fromEstacionadoAction.AddEstacionadoAction): Estacionado[] {
  return [...state, action.estacionado];
}
