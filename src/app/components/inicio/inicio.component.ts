import { Usuario } from './../../entities/usuario.entity';
import { Estacionamiento } from './../../entities/estacionamiento.entity';
import { UsuarioLogin } from './../../entities/usuarioLogin.entity';
import { CuposEstacionamiento } from 'app/entities/cuposEstacionamiento.entity';
import { Router } from '@angular/router';
import { AppState } from './../../app.state';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { UtilsService } from 'app/common/utilServices';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {


  constructor(
    private store: Store<AppState>,
    private router: Router,
    private utils: UtilsService
  ) { }

  ngOnInit(): void {
    //TODO: agregar
    this.store.select('usuarioLogin').subscribe(
      (data: UsuarioLogin) => {
        if (data == null) {
          this.router.navigate(['/login']);
        }
      })

    
  }

}
