import { UsuarioLogin } from '../../../entities/usuarioLogin.entity';
import { Router } from '@angular/router';
import { AppState } from '../../../app.state';
import { AdministradorService } from '../../../service/administrador/administrador.service';
import { ThemePalette } from '@angular/material/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

declare var $:any;

@Component({
  selector: 'app-crear-administrador',
  templateUrl: './crear-administrador.component.html',
  styleUrls: ['./crear-administrador.component.css']
})
export class CrearAdministradorComponent implements OnInit {

    //CONFIGURACION DEL FORMULARIO
    form: FormGroup

    //CONFIGURACION DEL TOOGLE
    color: ThemePalette = 'primary';

  constructor(
    private formBuilder: FormBuilder,
    private administradorService: AdministradorService,
    private store: Store<AppState>,
    private router: Router,
  ) {
    this.buildForm();
   }

  ngOnInit(): void {
    this.store.select('usuarioLogin').subscribe(
      (data: UsuarioLogin) => {
        if(data==null) this.router.navigate(['/login']);
      })
  }

  private buildForm() {
    this.form = this.formBuilder.group({
      correo: ['', [Validators.required]],
      habilitado: ['', [Validators.required]],

    })
  }

  guardar(event: Event) {
    event.preventDefault();
    if(this.form.valid){
      const value = this.form.value;
      this.administradorService.agregarAdminitrador(value);
      this.form.reset();
      this.showNotification('top', 'center');
    }else{
      this.form.markAllAsTouched();
    }
  }

  showNotification(from, align){
    $.notify({
        icon: "pe-7s-like2",
        message: "<b>Éxito</b> estacionamiento agregado."
    },{
        type: 'success',
        timer: 1000,
        placement: {
            from: from,
            align: align
        }
    });
}

}
