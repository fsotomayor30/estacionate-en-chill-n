import { AppState } from '../../app.state';
import { Router } from '@angular/router';
import { Administrador } from '../../entities/administrador.entity';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-administradores',
  templateUrl: './administradores.component.html',
  styleUrls: ['./administradores.component.css']
})
export class AdministradoresComponent implements OnInit {

  administradores: Administrador[] = [];


  constructor(
    private store: Store<AppState>,
    private router: Router,
  ) { }

  ngOnInit(): void {
        //TODO: agregar
      /*     this.store.select('user').subscribe(
      (data: User) => {
        if(data==null) this.router.navigate(['/login']);
      }) */

      this.store.select('administradores').subscribe(
        (data: Administrador[]) => {
            this.administradores=data;
        })
  }

  editar(id:string){
    this.router.navigate(['/editar-administrador', id])
  }

}
