import { Administrador } from '../../../entities/administrador.entity';
import * as fromAdministradorAction from '../actions/administrador.action';

const intialStateAdministrador:Administrador[] = [];

export function administradorReducer(state: Administrador[] = intialStateAdministrador, action: any): Administrador[] {
  switch (action.type) {
    case fromAdministradorAction.UPDATE_ADMINISTRADOR:
      return _mapUpdateAdministradorActionToState(state, action);
    case fromAdministradorAction.ADD_ADMINISTRADOR:
      return _mapAddAdministradorActionToState(state, action);
    default:
      return state;

  }
}

function _mapUpdateAdministradorActionToState(state: Administrador[], action: fromAdministradorAction.UpdateAdministradorAction): Administrador[] {
  return action.administradores;
}

function _mapAddAdministradorActionToState(state: Administrador[], action: fromAdministradorAction.AddAdministradorAction): Administrador[] {
  return [...state, action.administrador];
}
