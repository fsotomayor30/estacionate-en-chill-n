import { AdministradorService } from '../../../service/administrador/administrador.service';
import { AppState } from '../../../app.state';
import { ActivatedRoute } from '@angular/router';
import { Administrador } from '../../../entities/administrador.entity';
import { ThemePalette } from '@angular/material/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

declare var $: any;

@Component({
  selector: 'app-editar-administrador',
  templateUrl: './editar-administrador.component.html',
  styleUrls: ['./editar-administrador.component.css']
})
export class EditarAdministradorComponent implements OnInit {

  //CONFIGURACION DEL FORMULARIO
  form: FormGroup

  //CONFIGURACION DEL TOOGLE
  color: ThemePalette = 'primary';

  private administradores: Administrador[] = [];
  private administrador: Administrador;
  private idValido: boolean;
  private id: string;

  constructor(
    private route: ActivatedRoute,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private administradorService: AdministradorService
  ) { 
    this.buildForm();
  }

  ngOnInit(): void {
        //TODO: agregar
      /*     this.store.select('user').subscribe(
      (data: User) => {
        if(data==null) this.router.navigate(['/login']);
      }) */
      
    this.route.paramMap.subscribe(params => {
      if (params.has("id")) {
        this.id = params.get("id");
        this.idValido = true;
        this.store.select("administradores").subscribe(
          (data: Administrador[]) => {
            this.administradores = data
          }
        )
        this.administrador = this.administradores.find(e => e.id == this.id);
        if (this.administrador == undefined) {
          this.idValido = false;
          this.desactivarForm();
        } else {
          this.form.setValue(this.administrador);
        }
      }
    })
  }

  private buildForm() {
    this.form = this.formBuilder.group({
      id: ['', [Validators.required]],
      correo: ['', [Validators.required]],
      habilitado: ['', [Validators.required]],

    })
  }

  private desactivarForm(){
    this.form.controls['correo'].disable();
    this.form.controls['habilitado'].disable();
  }

  guardar(event: Event) {
    event.preventDefault();
    if (this.form.valid) {
      const value = this.form.value;
      this.administradorService.editarAdministrador(value);
      this.form.reset;
      this.showNotificationExito('top', 'center', "<b>Éxito</b> administrador actualizado.");
    } else {
      this.form.markAllAsTouched();
    }
  }

  showNotificationExito(from, align, mensaje) {
    $.notify({
      icon: "pe-7s-like2",
      message: mensaje
    }, {
      type: 'success',
      timer: 1000,
      placement: {
        from: from,
        align: align
      }
    });
  }

}
