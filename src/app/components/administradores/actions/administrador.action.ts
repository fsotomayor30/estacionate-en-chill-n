import { Administrador } from '../../../entities/administrador.entity';

export const UPDATE_ADMINISTRADOR = '[ADMINISTRADOR] UPDATE_ADMINISTRADOR';
export const ADD_ADMINISTRADOR = '[ADMINISTRADOR] ADD_ADMINISTRADOR';

export class UpdateAdministradorAction {
  type: string = UPDATE_ADMINISTRADOR;
  administradores: Administrador[];

  constructor(administradores: Administrador[]) {
    this.administradores = administradores;
  }
}

export class AddAdministradorAction {
  type: string = ADD_ADMINISTRADOR;
  administrador: Administrador;

  constructor(administrador: Administrador) {
    this.administrador = administrador;
  }
}


