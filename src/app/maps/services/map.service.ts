import { Estacionamiento } from './../../entities/estacionamiento.entity';
import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';
import "mapbox-gl/dist/mapbox-gl.css";

@Injectable({
  providedIn: 'root'
})
export class MapService {
  mapbox = (mapboxgl as typeof mapboxgl);
  map: mapboxgl.Map;
  style = `mapbox://styles/mapbox/streets-v11`;
  // Coordenadas de la localización donde queremos centrar el mapa
  lat = -36.6066399;
  lng = -72.1034393;
  zoom = 15;

  constructor() {
    // Asignamos el token desde las variables de entorno
    this.mapbox.accessToken = environment.mapBoxToken;
  }
  buildMap(puntos: Estacionamiento[]) {

    this.map = new mapboxgl.Map({
      container: 'map',
      style: this.style,
      zoom: this.zoom,
      scrollZoom: false,
      center: [this.lng, this.lat],
    });

    this.map.addControl(new mapboxgl.NavigationControl({
      showCompass:false
    }), "top-left");
    puntos.forEach(estacionamiento=>{
      if(estacionamiento.longitud != null && estacionamiento.latitud != null){
         new mapboxgl.Marker()
        .setLngLat([+estacionamiento.longitud, +estacionamiento.latitud])
        .setPopup(new mapboxgl.Popup({ offset: 25 }) // add popups
        .setHTML('<h3>' + estacionamiento.nombre + '</h3>'))
        .addTo(this.map)     
      }

    })
  }
}