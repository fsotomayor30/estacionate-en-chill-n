import { AppState } from './../app.state';
import { Router } from '@angular/router';
import { EstacionamientoService } from './../service/estacionamiento/estacionamiento.service';
import { MapService } from './services/map.service';
import { Estacionamiento } from './../entities/estacionamiento.entity';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css']
})

export class MapsComponent implements OnInit {

  public estacionamientos: Array<Estacionamiento> = new Array();

  constructor(
    private map: MapService, 
    private store: Store<AppState>) { 
    
  }
  ngOnInit() {
        //TODO: agregar
      /*     this.store.select('user').subscribe(
      (data: User) => {
        if(data==null) this.router.navigate(['/login']);
      }) */

      this.store.select('estacionamientos').subscribe(
        (data: Estacionamiento[]) => {
            this.estacionamientos=data;
            this.map.buildMap(this.estacionamientos);
        })

  }
}
