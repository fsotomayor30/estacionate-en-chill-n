import { Estacionado } from 'app/entities/estacionado.entity';
import { CuposEstacionamiento } from './entities/cuposEstacionamiento.entity';
import { Usuario } from './entities/usuario.entity';
import { Busqueda } from './entities/busqueda.entity';
import { Administrador } from './entities/administrador.entity';
import { Estacionamiento } from './entities/estacionamiento.entity';
import { Simulacion } from './entities/simulacion.entity';
import { UsuarioLogin } from './entities/usuarioLogin.entity';
import { ActionReducerMap } from "@ngrx/store";

import * as fromUserReducer from '../app/components/login/reducers/user.reducer';
import * as fromEstacionamientoReducer from './components/estacionamientos/reducers/estacionamiento.reducer';
import * as fromAdministradorReducer from './components/administradores/reducers/administrador.reducer';
import * as fromBusquedasReducer from './components/busquedas/reducers/busqueda.reducer';
import * as fromSimulacionReducer from './components/simulaciones/reducers/simulacion.reducer';
import * as fromUsuarioReducer from './components/usuarios/reducers/usuario.reducer';
import * as fromCupoEstacionamientoReducer from './components/cupos-estacionamiento/reducers/cuposEstacionamiento.reducer';
import * as fromEstacionadoReducer from './components/cupos-estacionamiento/reducers/estacionado.reducer';


export const appCombineReducers: ActionReducerMap<AppState> = {
    usuarioLogin:               fromUserReducer.userReducer,
    estacionamientos:           fromEstacionamientoReducer.estacionamientoReducer,
    administradores:            fromAdministradorReducer.administradorReducer,
    busquedas:                  fromBusquedasReducer.busquedaReducer,
    simulaciones:               fromSimulacionReducer.simulacionReducer,
    usuarios:                   fromUsuarioReducer.userReducer,
    cuposEstacionamientos:      fromCupoEstacionamientoReducer.estacionamientoReducer,
    estacionados:               fromEstacionadoReducer.estacionadoReducer
}

export interface AppState {
    usuarioLogin:           UsuarioLogin;
    estacionamientos:       Estacionamiento[];
    administradores:        Administrador[];
    busquedas:              Busqueda[];
    simulaciones:           Simulacion[];
    usuarios:               Usuario[],
    cuposEstacionamientos:  CuposEstacionamiento[],
    estacionados:           Estacionado[]
}