export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "AIzaSyB96ywaFquq1L59mfJ_EU7Pg1Vk3GM81Lc",
    authDomain: "estacionate-en-chillan.firebaseapp.com",
    databaseURL: "https://estacionate-en-chillan.firebaseio.com",
    projectId: "estacionate-en-chillan",
    storageBucket: "estacionate-en-chillan.appspot.com",
    messagingSenderId: "269856314959",
    appId: "1:269856314959:web:75c67af98d7df19534bc3a"
  },
  mapBoxToken: 'pk.eyJ1IjoiZXN0YWNpb25hdGVlbmNoaWxsYW4iLCJhIjoiY2s4ZGltaW5mMHNkbzNsbnB1czhidmo1eCJ9.5y_8XqvC--TFuM92U_6N6Q'

};
